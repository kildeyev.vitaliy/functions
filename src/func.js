const getSum = (str1, str2) => {
  let num1 = Number(str1);
  let num2 = Number(str2);
  if (typeof str1 !== 'string' || typeof str2 !== 'string' ||
    isNaN(num1) || isNaN(num2)) return false;
  return (num1 + num2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount = 0;
  let commentsCount = 0;
  for (const post of listOfPosts) {
    if (post.author === authorName) postsCount++;
    if (post.comments !== undefined) {
      for (const comment of post.comments) {
        if (comment.author === authorName) commentsCount++;
      }
    }
  }
  return `Post:${postsCount},comments:${commentsCount}`;
};

const tickets=(people)=> {
  const price = 25;
  let b, cashbox = 0;
  for (const bill of people) {
    b = Number(bill);
    if (b == price) {
      cashbox += price;
    } else if (b > price) {
      if ((b - price) > cashbox) return 'NO';
      cashbox += price;
    } else return 'NO';
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
